import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LibService } from '../lib.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit, OnDestroy {

  book = {};
  bookId = 0;

  private bookId$: Subscription;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private bookSvc: LibService) { }

  ngOnInit() {
    console.log("BookDetail ngOnInit: ")
    //we're taking a snapshot. snapshot do not change
    //this.bookId = parseInt(this.activatedRoute.snapshot.params.bookId);
    this.bookId$ = this.activatedRoute.params.subscribe(
      (param) => {
        console.log('> param  = ', param);
        this.bookId = parseInt(param.bookId);
        this.bookSvc.getBook(this.bookId)
          .then((result) => this.book = result)
          .catch((error) => {
            alert(`Error: ${JSON.stringify(error)}`);
          });
      }
    )
  }

  ngOnDestroy() {
    this.bookId$.unsubscribe();
  }

  prev() {
    this.bookId = this.bookId - 1;
    this.router.navigate(['/book', this.bookId])
  }

  next() {
    this.bookId = this.bookId + 1;
    this.router.navigate(['/book', this.bookId])
  }

  goBack() {
    this.router.navigate(['/' ]);
  }

}
