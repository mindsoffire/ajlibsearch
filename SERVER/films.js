// load libraries needed
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql');

// instantiate express-application obj
const app = express();
// use CORS middleware
app.use(cors());

// e.g. npm run main.js 6000 ; nodemon main.js 6500 ; node main.js 6500 ; nodemon cart.js 6501
app.set('port', process.argv[2] || process.env.PORT || 3000);

var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family +
        ')127.0.0.1:' + server.address().address + server.address().port); // to find what y server.address().address don't work
});

// load MySQL config file
const mySQLConfig = require('./mysql-config');
const pool = mysql.createPool(mySQLConfig);

// call mkQuery to handle SQL API
const mkQuery = (SQL, pool) => {
    return (params) => {
        const p = new Promise((resolve, reject) => {
            console.log("In closure: ", SQL);
            pool.getConnection((err, conn) => {
                if (err) {
                    reject({ status: 500, error: err }); return;
                    // return a reject to getConnection from pool
                }
                conn.query(SQL, params || [],
                    (err, result) => {
                        try {
                            if (err)
                                reject(err);
                            // reject query bcos of error
                            else {
                                console.info(result);
                                /* reject({ status: 400, error: err }); return; */
                                resolve(result);
                            }
                        } finally {
                            conn.release();
                        }
                    }
                )
            })
        })
        return (p); // return Promise to params callback
    }               // return a params function 
}

// set up route & handler for getting all films loaded
const SELECT_FILMS = 'select film_id, title from film limit ? offset ?';
const selectFilms = mkQuery(SELECT_FILMS, pool);   // instantiate params function object that closed mkQuery
app.get('/films', (req, res) => {
    const limit = req.query.limit || 10;
    const offset = req.query.offset || 0;

    selectFilms([parseInt(limit), parseInt(offset)])
        .then(result => { console.log(result); res.status(200).json(result); })
        .catch(err => { });
});

// route & handler for selecting film by id
const SELECT_BY_FILM_ID = 'select * from film where film_id = ?';
const selectByFilmId = mkQuery(SELECT_BY_FILM_ID, pool);
app.get('/film/:filmId', (req, res) => {
    console.log(`film id = ${req.params.filmId}`);

    selectByFilmId([req.params.filmId])
        .then(result => {
            if (result.length)
                res.status(200).json(result[0]);
            else
                res.status(404).json({ error: "Not found" });
        });
});
